<?php

// src/AppBundle/Entity/Bid.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="bid")
 */
class Bid {
    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="buyer", length=100)
     */
    protected $email;

    /**
     * @ORM\Column(type="decimal", name="offer", scale=2)
     */
    protected $offer;

    /**
     * @ORM\Column(type="string", name="event", length=100)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", name="eventDate", length=10)
     */
    protected $date;

    /**
     * @ORM\Column(type="string", name="ticketType", length=100)
     */
    protected $type;

    /**
     * @ORM\Column(type="decimal", name="price", scale=2)
     */
    protected $price;

    /**
     * @ORM\Column(type="string", name="seat", length=100)
     */
    protected $seat;

    // getters
    public function getId() {
        return $this->id;
    }

    public function getBuyer() {
        return $this->email;
    }

    public function getOffer() {
        return $this->offer;
    }

    public function getEvent() {
        return $this->title;
    }

    public function getEventDate() {
        return $this->date;
    }

    public function getTicketType() {
        return $this->type;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getSeat() {
        return $this->seat;
    }

    // setters
    public function setId($property) {
        $this->id=$property;
    }

    public function setBuyer($property) {
        $this->email=$property;
    }

    public function setOffer($property) {
        $this->offer=$property;
    }

    public function setEvent($property) {
        $this->title=$property;
    }

    public function setEventDate($property) {
        $this->date=$property;
    }

    public function setTicketType($property) {
        $this->type=$property;
    }

    public function setPrice($property) {
        $this->price=$property;
    }

    public function setSeat($property) {
        $this->seat = $property;
    }

}

?>