<?php

// src/AppBundle/Entity/UserType.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User {
    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="email", length=100)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", name="password", length=100)
     */
    protected $password;

    // getters
    public function getId() {
        return $this->id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPassword() {
        return $this->password;
    }

    // setters
    public function setId($property) {
        $this->id=$property;
    }

    public function setEmail($property) {
        $this->email=$property;
    }

    public function setPassword($property) {
        $this->password=$property;
    }
}

?>