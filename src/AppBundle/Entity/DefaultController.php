<?php
// src/AppBundle/Controller/DefaultController.php

// ...
use AppBundle\Entity\Ticket;
use AppBundle\Entity\Book;
use Symfony\Component\HttpFoundation\Response;

// select all tickets sorted by price in ascending order
function request_ticket() {
	$repository = $this->getDoctrine()->getRepository('AppBundle:Ticket');
	$tickets = $repository->findBy(
	    array('ticketType' => 'Ticket', 'available' => 'Yes'),
	    array('price' => 'ASC')
	);

    if (!$tickets) {
        throw $this->createNotFoundException(
            'No ticket found'
        );
    }
}

// create ticket
function create_ticket($name, $event_date, $price, $seat, $contact, $posted_date) {
    $ticket = new Ticket();
    $ticket->setEvent($name);
    $ticket->setEventDate($event_date);
    $ticket->setTicketType('Ticket');
    $ticket->setPrice($price);
    $ticket->setSeat($seat);
    $ticket->setSeller($contact);
    $ticket->setPostedDate($posted_date);
    $ticket->setAvailable('Yes');

    $em = $this->getDoctrine()->getManager();

    $em->persist($ticket);
    $em->flush();

    return new Response('Created product id ' . $ticket->getId());
}

// select all books sorted by price in ascending order
function request_book() {
	$repository = $this->getDoctrine()->getRepository('AppBundle:Book');
	$books = $repository->findBy(
	    array('type' => 'Book', 'available' => 'Yes'),
	    array('price' => 'ASC')
	);

    if (!$books) {
        throw $this->createNotFoundException(
            'No book found'
        );
    }
}

// create book
function create_book($title, $isbn, $desc, $price, $contact, $posted_date) {
    $book = new Book();
    $book->setTitle($title);
    $book->setIsbn($isbn);
    $book->setDescription($desc);
    $book->setType('Book');
    $book->setPrice($price);
    $book->setSeller($contact);
    $book->setPostedDate($posted_date);
    $book->setAvailable('Yes');

    $em = $this->getDoctrine()->getManager();

    $em->persist($book);
    $em->flush();

    return new Response('Created product id ' . $book->getId());
}

// select all leases sorted by price in ascending order
function request_lease() {
	$repository = $this->getDoctrine()->getRepository('AppBundle:Lease');
	$leases = $repository->findBy(
	    array('type' => 'Lease', 'available' => 'Yes'),
	    array('price' => 'ASC')
	);

    if (!$leases) {
        throw $this->createNotFoundException(
            'No lease found'
        );
    }
}

// create lease
function create_lease($address, $date, $style, $rooms, $desc, $price, $contact, $posted_date) {
    $lease = new Lease();
    $lease->setAddress($address);
    $lease->setLeaseDate($date);
    $lease->setLeaseStyle($style);
    $lease->setRooms($rooms);
    $lease->setDescription($desc);
    $lease->setType('Book');
    $lease->setPrice($price);
    $lease->setSeller($contact);
    $lease->setPostedDate($posted_date);
    $lease->setAvailable('Yes');

    $em = $this->getDoctrine()->getManager();

    $em->persist($lease);
    $em->flush();

    return new Response('Created product id ' . $lease->getId());
}

// select all sales
function see_my_sales($email) {
	$repository = $this->getDoctrine()->getRepository('AppBundle:Lease');
	$leases = $repository->findBy(
	    array('type' => 'Lease', 'available' => 'Yes', 'seller' => $email)
	);

	$repository = $this->getDoctrine()->getRepository('AppBundle:Book');
	$books = $repository->findBy(
	    array('type' => 'Book', 'available' => 'Yes', 'seller' => $email)
	);

	$repository = $this->getDoctrine()->getRepository('AppBundle:Ticket');
	$tickets = $repository->findBy(
	    array('ticketType' => 'Ticket', 'available' => 'Yes', 'seller' => $email)
	);
}

// create bid
function create_bid($id, $email, $offer, $title, $date, $type, $price, $seat) {
    $bid = new Bid();
    $bid->setID($id);
    $bid->setBuyer($email);
    $bid->setOffer($offer);
    $bid->setEvent($title);
    $bid->setEventDate($date);
    $bid->setTicketType($type);
    $bid->setPrice($price);
    $bid->setSeat($seat);

    $em = $this->getDoctrine()->getManager();

    $em->persist($bid);
    $em->flush();

    return new Response('Created product id ' . $bid->getId());
}

// select all bids
function see_my_bids($email) {
    $repository = $this->getDoctrine()->getRepository('AppBundle:Bid');
    $bids = $repository->findBy(
        array('buyer' => $email)
    );

    if (!$bids) {
        throw $this->createNotFoundException(
            'No bids found'
        );
    }
}

// check bid ticket
function check_ticket_bid($uniqueID, $email) {
    $repository = $this->getDoctrine()->getRepository('AppBundle:Bid');
    $bids = $repository->findBy(
        array('buyer' => $email, 'ID' => $uniqueID)
    );

    if (!$bids) {
        throw $this->createNotFoundException(
            'No bids found'
        );
    }
}

// see bids
function see_bids($id) {
    $repository = $this->getDoctrine()->getRepository('AppBundle:Bid');
    $bids = $repository->findBy(
        array('id' => $id)
    );

    if (!$bids) {
        throw $this->createNotFoundException(
            'No bids found'
        );
    }
}

// request specific ticket
function request_specific_ticket($uniqueID) {
    $repository = $this->getDoctrine()->getRepository('AppBundle:Ticket');
    $tickets = $repository->findBy(
        array('id' => $uniqueID)
    );

    if (!$tickets) {
        throw $this->createNotFoundException(
            'No ticket found'
        );
    }
}

// request specific book
function request_specific_book($uniqueID) {
    $repository = $this->getDoctrine()->getRepository('AppBundle:Book');
    $books = $repository->findBy(
        array('id' => $uniqueID)
    );

    if (!$books) {
        throw $this->createNotFoundException(
            'No book found'
        );
    }
}

// request specific lease
function request_specific_lease($uniqueID) {
    $repository = $this->getDoctrine()->getRepository('AppBundle:Lease');
    $leases = $repository->findBy(
        array('id' => $uniqueID)
    );

    if (!$leases) {
        throw $this->createNotFoundException(
            'No lease found'
        );
    }
}

// search tickets
function search_tickets($event_title) {
    $repository = $this->getDoctrine()->getRepository('AppBundle:Ticket');
    $tickets = $repository->findBy(
        array('event' => $event_title)
    );

    if (!$tickets) {
        throw $this->createNotFoundException(
            'No ticket found'
        );
    }
}

?>