<?php

// src/AppBundle/Entity/Adbook.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="adbook")
 */
class Adbook {
    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="title", length=100)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", name="isbn", length=50)
     */
    protected $isbn;

    /**
     * @ORM\Column(type="text", name="description")
     */
    protected $description;

    /**
     * @ORM\Column(type="string", name="type", length=100)
     */
    protected $type;

    /**
     * @ORM\Column(type="decimal", name="price", scale=2)
     */
    protected $price;

    /**
     * @ORM\Column(type="string", name="seller", length=100)
     */
    protected $seller;

    /**
     * @ORM\Column(type="string", name="postedDate", length=10)
     */
    protected $postedDate;

    /**
     * @ORM\Column(type="string", name="available", length=5)
     */
    protected $available;

    /**
     * Adbook constructor.
     * @param string $title
     * @param string $isbn
     * @param string $description
     * @param string $type
     * @param float $price
     * @param string $seller
     * @param string $postedDate
     * @param string $available
     */
    public function __construct($title = '', $isbn = '', $description = '', $type = '', $price = 0.00, $seller = '', $postedDate = '', $available = '')
    {
        $this->title = $title;
        $this->isbn = $isbn;
        $this->description = $description;
        $this->type = $type;
        $this->price = $price;
        $this->seller = $seller;
        $this->postedDate = $postedDate;
        $this->available = $available;
    }

    // getters
    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getIsbn() {
        return $this->isbn;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getType() {
        return $this->type;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getSeller() {
        return $this->seller;
    }

    public function getPostedDate() {
        return $this->postedDate;
    }

    public function getAvailable() {
        return $this->available;
    }

    // setters
    public function setId($property) {
        $this->id=$property;
    }

    public function setTitle($property) {
        $this->title=$property;
    }

    public function setIsbn($property) {
        $this->isbn=$property;
    }

    public function setDescription($property) {
        $this->description=$property;
    }

    public function setType($property) {
        $this->type=$property;
    }

    public function setPrice($property) {
        $this->price=$property;
    }

    public function setSeller($property) {
        $this->seller=$property;
    }

    public function setPostedDate($property) {
        $this->postedDate = $property;
    }

    public function setAvailable($property) {
        $this->available=$property;
    }

}

?>