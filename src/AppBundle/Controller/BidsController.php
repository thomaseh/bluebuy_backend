<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\Bid;
use AppBundle\Form\BidType;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BidsController extends FOSRestController
{
    /**
     *
     * @return AdTicket
     */
    public function getBidAction(Bid $bid)
    {
        return $bid;
    }
    /**
     * @param $name
     * @return Bid
     */
    public function getBidNameAction($name) {
        $bid = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Bid')
            ->findByTitle($name);
        return $bid;
    }
    /**
     * retrieve all bids
     * TODO: add pagination
     *
     * @return Bid[]
     */
    public function getBidsAction()
    {
        $bids = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Bid')
            ->findAll();
        return $bids;
    }

    /**
     * @param $email
     * @return \AppBundle\Entity\Bid[]|array
     */
    public function getBidsEmailAction($email)
    {
        $bids = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Bid')
            ->findBy(array('email' => $email), array('price' => 'ASC'));
        return $bids;
    }

    /**
     * @param $id
     * @param $email
     * @return \AppBundle\Entity\Bid[]|array
     */
    public function getBidIdEmailAction($id, $email)
    {
        $bids = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Bid')
            ->findBy(array('id' => $id,'email' => $email));
        return $bids;
    }

    /**
     * @return \AppBundle\Entity\Bid[]|array
     */
    public function getBidsSortbyTitleAction() {
        $bids = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Bid')
            ->findBy(array(), array('title' => 'ASC'));
        return $bids;
    }

    /**
     * @return \AppBundle\Entity\Bid[]|array
     */
    public function getBidsSortbyDateAction() {
        $bids = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Bid')
            ->findBy(array(), array('date' => 'ASC'));
        return $bids;
    }

    /**
     * @return \AppBundle\Entity\Bid[]|array
     */
    public function getBidsSortbyPriceAction() {
        $bids = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Bid')
            ->findBy(array(), array('price' => 'ASC'));
        return $bids;
    }

    /**
     * @param Request $request
     * @return View
     */
    public function postBidsAction(Request $request)
    {
        //TODO: there's a simpler method using FOSRestBundle body converter
        // that's the reason why we need to be able to create
        // an bid without body or title, to use it as
        // a placeholder for the form
        $bid = new Bid();
        $errors = $this->treatAndValidateRequest($bid, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->persistAndFlush($bid);
        // created => 201, we need View here because we're not
        // returning the default 200
        return new View($bid, Response::HTTP_CREATED);
    }

    /**
     * @param Bid $bid
     * @param Request $request
     * @return View
     */
    public function putBidAction(Bid $bid, Request $request)
    {
        echo $request;
        // yes we replace totally the old bid by a new one
        // except the id, because that's how PUT works
        // if you just want to "merge" you want to use PATCH, not PUT
        $id = $bid->getId();
        $bid = new Bid();
        $bid->setId($id);
        $errors = $this->treatAndValidateRequest($bid, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->persistAndFlush($bid);
        return new View($bid, Response::HTTP_OK);
    }

    /**
     * @param Bid $bid
     * @param Request $request
     * @return View
     */
    public function deleteBidAction(Bid $bid, Request $request) {
        // delete
//        $id = $bid->getId();
//        $bid = new Bid();
//        $bid->setId($id);
        $errors = $this->treatAndValidateRequest($bid, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->deleteAndFlush($bid);
        return new View($bid, Response::HTTP_OK);
    }
    /**
     * fill $bid with the json send in request and validates it
     *
     * returns an array of errors (empty if everything is ok)
     *
     * @return array
     */
    private function treatAndValidateRequest(Bid $bid, Request $request)
    {
        // createForm is provided by the parent class
        $form = $this->createForm(
            'AppBundle\Form\BidType',
            $bid,
            array(
                'method' => $request->getMethod()
            )
        );
        // this method is the one that will use the value in the POST
        // to update $bid
        $form->handleRequest($request);

        // we use it like that instead of the standard $form->isValid()
        // because the json generated
        // is much readable than the one by serializing $form->getErrors()
        $errors = $this->get('validator')->validate($bid);
        return $errors;
    }
    /**
     * @param Bid $bid
     */
    private function persistAndFlush(Bid $bid)
    {
        $manager = $this->getDoctrine()->getManager();
        $id = $bid->getId();
        if (!is_numeric($id)) $id = -1;
        if ($id >= 0) {
            $product = $manager->getRepository('AppBundle:Bid')->find($id);

            if (!$product) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $product->setBuyer($bid->getBuyer());
            $product->setOffer($bid->getOffer());
            $product->setEvent($bid->getEvent());
            $product->setEventDate($bid->getEventDate());
            $product->setTicketType($bid->getTicketType());
            $product->setType($bid->getType());
            $product->setPrice($bid->getSeller());
            $product->setSeat($bid->getSeat());
            $manager->flush();
        } else {
            $manager->persist($bid);
            $manager->flush();
        }
    }

    /**
     * @param Bid $bid
     */
    private function deleteAndFlush(Bid $bid) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($bid);
        $manager->flush();
    }
}