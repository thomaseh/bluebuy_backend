<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdticketType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('event')
            ->add('eventDate')
            ->add('ticketType')
            ->add('price')
            ->add('seat')
            ->add('seller')
            ->add('postedDate')
            ->add('available')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'csrf_protection' => false,
                'data_class' => 'AppBundle\Entity\Adticket',
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        // this is important in order to be able
        // to provide the entity directly in the json
        return 'adticket';
    }
}
