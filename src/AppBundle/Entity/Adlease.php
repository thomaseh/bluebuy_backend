<?php

// src/AppBundle/Entity/Adlease.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="adlease")
 */
class Adlease {
    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="title", length=100)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", name="leaseDate", length=10)
     */
    protected $lease_date;

    /**
     * @ORM\Column(type="string", name="leaseStyle", length=50)
     */
    protected $lease_style;

    /**
     * @ORM\Column(type="string", name="rooms", length=100)
     */
    protected $rooms;

    /**
     * @ORM\Column(type="string", name="description", length=500)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", name="type", length=100)
     */
    protected $type;

    /**
     * @ORM\Column(type="decimal", name="price", scale=2)
     */
    protected $price;

    /**
     * @ORM\Column(type="string", name="seller", length=100)
     */
    protected $seller;

    /**
     * @ORM\Column(type="string", name="postedDate", length=10)
     */
    protected $posted_date;

    /**
     * @ORM\Column(type="string", name="available", length=5)
     */
    protected $available;

    /**
     * Adlease constructor.
     * @param string $title
     * @param string $lease_date
     * @param string $lease_style
     * @param string $rooms
     * @param string $description
     * @param string $type
     * @param float $price
     * @param string $seller
     * @param string $posted_date
     * @param string $available
     */
    public function __construct($title = '', $lease_date = '', $lease_style = '', $rooms = '', $description = '', $type = '', $price = 0.00, $seller = '', $posted_date = '', $available = '')
    {
        $this->title = $title;
        $this->lease_date = $lease_date;
        $this->lease_style = $lease_style;
        $this->rooms = $rooms;
        $this->description = $description;
        $this->type = $type;
        $this->price = $price;
        $this->seller = $seller;
        $this->posted_date = $posted_date;
        $this->available = $available;
    }

    // getters
    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getLeaseDate() {
        return $this->lease_date;
    }

    public function getLeaseStyle() {
        return $this->lease_style;
    }

    public function getRooms() {
        return $this->rooms;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getType() {
        return $this->type;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getSeller() {
        return $this->seller;
    }

    public function getPostedDate() {
        return $this->posted_date;
    }

    public function getAvailable() {
        return $this->available;
    }

    // setters
    public function setId($property) {
        $this->id=$property;
    }

    public function setTitle($property) {
        $this->title=$property;
    }

    public function setLeaseDate($property) {
        $this->lease_date=$property;
    }

    public function setLeaseStyle($property) {
        $this->lease_style=$property;
    }

    public function setRooms($property) {
        $this->rooms=$property;
    }

    public function setDescription($property) {
        $this->description=$property;
    }

    public function setType($property) {
        $this->type=$property;
    }

    public function setPrice($property) {
        $this->price=$property;
    }

    public function setSeller($property) {
        $this->seller=$property;
    }

    public function setPostedDate($property) {
        $this->posted_date=$property;
    }

    public function setAvailable($property) {
        $this->available=$property;
    }

}

?>