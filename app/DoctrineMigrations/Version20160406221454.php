<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160406221454 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE adTicket (id INT AUTO_INCREMENT NOT NULL, event VARCHAR(100) NOT NULL, eventDate VARCHAR(10) NOT NULL, ticketType VARCHAR(100) NOT NULL, price NUMERIC(10, 2) NOT NULL, seat VARCHAR(10) NOT NULL, seller VARCHAR(100) NOT NULL, postedDate VARCHAR(10) NOT NULL, available VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE ticket');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ticket (id INT AUTO_INCREMENT NOT NULL, Event VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, eventDate VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, ticketType VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, price NUMERIC(10, 2) NOT NULL, seat VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, seller VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, postedDate VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, available VARCHAR(5) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE adTicket');
    }
}
