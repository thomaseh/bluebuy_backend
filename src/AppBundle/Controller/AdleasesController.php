<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\Adlease;
use AppBundle\Form\AdleaseType;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdleasesController extends FOSRestController
{
    /**
     *
     * @return Adlease
     */
    public function getAdleaseAction(Adlease $adlease)
    {
        return $adlease;
    }
    /**
     * @param $name
     * @return Adlease
     */
    public function getAdleaseNameAction($name) {
        $adlease = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adlease')
            ->findBy(array('title' => $name, 'available' => 'Yes'));
        return $adlease;
    }
    /**
     * retrieve all adleases
     * TODO: add pagination
     *
     * @return Adlease[]
     */
    public function getAdleasesAction()
    {
        $adleases = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adlease')
            ->findBy(array('available' => 'Yes'));
        return $adleases;
    }

    /**
     * @return \AppBundle\Entity\Adlease[]|array
     */
    public function getAdleasesSortbyTitleAction() {
        $adleases = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adlease')
            ->findBy(array('available' => 'Yes'), array('title' => 'ASC'));
        return $adleases;
    }

    /**
     * @return \AppBundle\Entity\Adlease[]|array
     */
    public function getAdleasesSortbyDateAction() {
        $adleases = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adlease')
            ->findBy(array('available' => 'Yes'), array('lease_date' => 'ASC'));
        return $adleases;
    }

    /**
     * @return \AppBundle\Entity\Adlease[]|array
     */
    public function getAdleasesSortbyPriceAction() {
        $adleases = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adlease')
            ->findBy(array('available' => 'Yes'), array('price' => 'ASC'));
        return $adleases;
    }

    /**
     * @param Request $request
     * @return View
     */
    public function postAdleasesAction(Request $request)
    {
        //TODO: there's a simpler method using FOSRestBundle body converter
        // that's the reason why we need to be able to create
        // an adlease without body or title, to use it as
        // a placeholder for the form
        $adlease = new Adlease();
        $errors = $this->treatAndValidateRequest($adlease, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->persistAndFlush($adlease);
        // created => 201, we need View here because we're not
        // returning the default 200
        return new View($adlease, Response::HTTP_CREATED);
    }

    /**
     * @param Adlease $adlease
     * @param Request $request
     * @return View
     */
    public function putAdleaseAction(Adlease $adlease, Request $request)
    {
        echo $request;
        // yes we replace totally the old adlease by a new one
        // except the id, because that's how PUT works
        // if you just want to "merge" you want to use PATCH, not PUT
        $id = $adlease->getId();
        $adlease = new Adlease();
        $adlease->setId($id);
        $errors = $this->treatAndValidateRequest($adlease, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->persistAndFlush($adlease);
        return new View($adlease, Response::HTTP_OK);
    }

    /**
     * @param Adlease $adlease
     * @param Request $request
     * @return View
     */
    public function deleteAdleaseAction(Adlease $adlease, Request $request) {
        // delete
//        $id = $adlease->getId();
//        $adlease = new Adlease();
//        $adlease->setId($id);
        $errors = $this->treatAndValidateRequest($adlease, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->deleteAndFlush($adlease);
        return new View($adlease, Response::HTTP_OK);
    }
    /**
     * fill $adlease with the json send in request and validates it
     *
     * returns an array of errors (empty if everything is ok)
     *
     * @return array
     */
    private function treatAndValidateRequest(Adlease $adlease, Request $request)
    {
        // createForm is provided by the parent class
        $form = $this->createForm(
            'AppBundle\Form\AdleaseType',
            $adlease,
            array(
                'method' => $request->getMethod()
            )
        );
        // this method is the one that will use the value in the POST
        // to update $adlease
        $form->handleRequest($request);

        // we use it like that instead of the standard $form->isValid()
        // because the json generated
        // is much readable than the one by serializing $form->getErrors()
        $errors = $this->get('validator')->validate($adlease);
        return $errors;
    }
    /**
     * @param Adlease $adlease
     */
    private function persistAndFlush(Adlease $adlease)
    {
        $manager = $this->getDoctrine()->getManager();
        $id = $adlease->getId();
        if (!is_numeric($id)) $id = -1;
        if ($id >= 0) {
            $product = $manager->getRepository('AppBundle:Adlease')->find($id);

            if (!$product) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $product->setTitle($adlease->getTitle());
            $product->setLeaseDate($adlease->getLeaseDate());
            $product->setLeaseStyle($adlease->getLeaseStyle());
            $product->setRooms($adlease->getRooms());
            $product->setDescription($adlease->getDescription());
            $product->setType($adlease->getType());
            $product->setPrice($adlease->getSeller());
            $product->setSeller($adlease->getSeller());
            $product->setPostedDate($adlease->getPostedDate());
            $product->setAvailable($adlease->getAvailable());
            $manager->flush();
        } else {
            $manager->persist($adlease);
            $manager->flush();
        }
    }

    /**
     * @param Adlease $adlease
     */
    private function deleteAndFlush(Adlease $adlease) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($adlease);
        $manager->flush();
    }
}