<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\Adbook;
use AppBundle\Form\AdbookType;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdbooksController extends FOSRestController
{
    /**
     *
     * @return Adbook
     */
    public function getAdbookAction($id)
    {
        $adbook = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adbook')
            ->findBy(array('id' => $id, 'available' => 'Yes'));

        if (!$adbook) {
            throw $this->createNotFoundException(
                'No product found for id '. $id
            );
        }

        return $adbook;
    }

    /**
     * @param $name
     * @return Adbook
     */
    public function getAdbookNameAction($name) {
        $adbook = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adbook')
            ->findBy(array('title' => $name, 'available' => 'Yes'));
        return $adbook;
    }
    /**
     * retrieve all adbooks
     * TODO: add pagination
     *
     * @return Adbook[]
     */
    public function getAdbooksAction()
    {
        $adbooks = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adbook')
            ->findBy(array('available' => 'Yes'));
        return $adbooks;
    }

    /**
     * @return \AppBundle\Entity\Adbook[]|array
     */
    public function getAdbooksSortbyTitleAction() {
        $adbooks = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adbook')
            ->findBy(array('available' => 'Yes'), array('title' => 'ASC'));
        return $adbooks;
    }

    /**
     * @return \AppBundle\Entity\Adbook[]|array
     */
    public function getAdbooksSortbyDateAction() {
        $adbooks = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adbook')
            ->findBy(array('available' => 'Yes'), array('postedDate' => 'ASC'));
        return $adbooks;
    }

    /**
     * @return \AppBundle\Entity\Adbook[]|array
     */
    public function getAdbooksSortbyPriceAction() {
        $adbooks = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adbook')
            ->findBy(array('available' => 'Yes'), array('price' => 'ASC'));
        return $adbooks;
    }

    /**
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new page from the submitted data.",
     *   input = "AppBundle\Form\AdbookType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     * @param Request $request
     * @return View
     */
    public function postAdbooksAction(Request $request)
    {
        //TODO: there's a simpler method using FOSRestBundle body converter
        // that's the reason why we need to be able to create
        // an adbook without body or title, to use it as
        // a placeholder for the form
        $adbook = new Adbook();
        $errors = $this->treatAndValidateRequest($adbook, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->persistAndFlush($adbook);
        // created => 201, we need View here because we're not
        // returning the default 200
        return new View($adbook, Response::HTTP_CREATED);
    }

    /**
     * @param Adbook $adbook
     * @param Request $request
     * @return View
     */
    public function putAdbookAction(Adbook $adbook, Request $request)
    {
        echo $request;
        // yes we replace totally the old adbook by a new one
        // except the id, because that's how PUT works
        // if you just want to "merge" you want to use PATCH, not PUT
        $id = $adbook->getId();
        $adbook = new Adbook();
        $adbook->setId($id);
        $errors = $this->treatAndValidateRequest($adbook, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->persistAndFlush($adbook);
        return new View($adbook, Response::HTTP_OK);
    }

    /**
     * @param Adbook $adbook
     * @param Request $request
     * @return View
     */
    public function deleteAdbookAction(Adbook $adbook, Request $request) {
        $errors = $this->treatAndValidateRequest($adbook, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->deleteAndFlush($adbook);
        return new View($adbook, Response::HTTP_OK);
    }
    /**
     * fill $adbook with the json send in request and validates it
     *
     * returns an array of errors (empty if everything is ok)
     *
     * @return array
     */
    private function treatAndValidateRequest(Adbook $adbook, Request $request)
    {
        // createForm is provided by the parent class
        $form = $this->createForm(
            'AppBundle\Form\AdbookType',
            $adbook,
            array(
                'method' => $request->getMethod()
            )
        );
        // this method is the one that will use the value in the POST
        // to update $adbook
        $form->handleRequest($request);

        // we use it like that instead of the standard $form->isValid()
        // because the json generated
        // is much readable than the one by serializing $form->getErrors()
        $errors = $this->get('validator')->validate($adbook);
        return $errors;
    }
    /**
     * @param Adbook $adbook
     */
    private function persistAndFlush(Adbook $adbook)
    {
        $manager = $this->getDoctrine()->getManager();
        $id = $adbook->getId();
        if (!is_numeric($id)) $id = -1;
        if ($id >= 0) {
            $product = $manager->getRepository('AppBundle:Adbook')->find($id);

            if (!$product) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $product->setTitle($adbook->getTitle());
            $product->setIsbn($adbook->getIsbn());
            $product->setDescription($adbook->getDescription());
            $product->setType($adbook->getType());
            $product->setPrice($adbook->getPrice());
            $product->setSeller($adbook->getSeller());
            $product->setPostedDate($adbook->getPostedDate());
            $product->setAvailable($adbook->getAvailable());
            $manager->flush();
        } else {
            echo "error";
            $manager->persist($adbook);
            echo "error1";
            $manager->flush();
        }
    }

    /**
     * @param Adbook $adbook
     */
    private function deleteAndFlush(Adbook $adbook) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($adbook);
        $manager->flush();
    }
}