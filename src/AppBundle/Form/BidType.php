<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BidType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('buyer')
            ->add('offer')
            ->add('event')
            ->add('event_date')
            ->add('ticket_type')
            ->add('price')
            ->add('seat')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'csrf_protection' => false,
                'data_class' => 'AppBundle\Entity\Bid',
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        // this is important in order to be able
        // to provide the entity directly in the json
        return 'bid';
    }
}
