<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160331230443 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE book (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(100) NOT NULL, isbn VARCHAR(50) NOT NULL, description VARCHAR(500) NOT NULL, type VARCHAR(100) NOT NULL, price NUMERIC(10, 2) NOT NULL, seller VARCHAR(100) NOT NULL, postedDate VARCHAR(10) NOT NULL, available VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lease (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(100) NOT NULL, leaseDate VARCHAR(10) NOT NULL, leaseStyle VARCHAR(50) NOT NULL, rooms VARCHAR(100) NOT NULL, description VARCHAR(500) NOT NULL, type VARCHAR(100) NOT NULL, price NUMERIC(10, 2) NOT NULL, seller VARCHAR(100) NOT NULL, postedDate VARCHAR(10) NOT NULL, available VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ticket (id INT AUTO_INCREMENT NOT NULL, Event VARCHAR(100) NOT NULL, eventDate VARCHAR(10) NOT NULL, ticketType VARCHAR(100) NOT NULL, price NUMERIC(10, 2) NOT NULL, seat VARCHAR(10) NOT NULL, seller VARCHAR(100) NOT NULL, postedDate VARCHAR(10) NOT NULL, available VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bid (id INT AUTO_INCREMENT NOT NULL, buyer VARCHAR(100) NOT NULL, offer VARCHAR(50) NOT NULL, event VARCHAR(100) NOT NULL, eventDate VARCHAR(10) NOT NULL, ticketType VARCHAR(100) NOT NULL, price NUMERIC(10, 2) NOT NULL, seat VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE book');
        $this->addSql('DROP TABLE lease');
        $this->addSql('DROP TABLE ticket');
        $this->addSql('DROP TABLE bid');
    }
}
