<?php

// src/AppBundle/Entity/Adticket.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="adticket")
 */
class Adticket {
    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="event", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="eventDate", length=10)
     */
    protected $event_date;

    /**
     * @ORM\Column(type="string", name="ticketType", length=100)
     */
    protected $type;

    /**
     * @ORM\Column(type="decimal", name="price", scale=2)
     */
    protected $price;

    /**
     * @ORM\Column(type="string", name="seat", length=10)
     */
    protected $seat;

    /**
     * @ORM\Column(type="string", name="seller", length=100)
     */
    protected $seller;

    /**
     * @ORM\Column(type="string", name="postedDate", length=10)
     */
    protected $posted_date;

    /**
     * @ORM\Column(type="string", name="available", length=5)
     */
    protected $available;

    /**
     * Adticket constructor.
     * @param string $name
     * @param string $event_date
     * @param string $type
     * @param float $price
     * @param string $seat
     * @param string $seller
     * @param string $posted_date
     * @param string $available
     */
    public function __construct($name = '', $event_date = '', $type = '', $price = 0.00, $seat = '', $seller = '', $posted_date = '', $available = '')
    {
        $this->name = $name;
        $this->event_date = $event_date;
        $this->type = $type;
        $this->price = $price;
        $this->seat = $seat;
        $this->seller = $seller;
        $this->posted_date = $posted_date;
        $this->available = $available;
    }

    // getters
    public function getId() {
        return $this->id;
    }

    public function getEvent() {
        return $this->name;
    }

    public function getEventDate() {
        return $this->event_date;
    }

    public function getTicketType() {
        return $this->type;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getSeat() {
        return $this->seat;
    }

    public function getSeller() {
        return $this->seller;
    }

    public function getPostedDate() {
        return $this->posted_date;
    }

    public function getAvailable() {
        return $this->available;
    }

    // setters
    public function setId($property) {
        $this->id=$property;
    }

    public function setEvent($property) {
        $this->name=$property;
    }

    public function setEventDate($property) {
        $this->event_date=$property;
    }

    public function setTicketType($property) {
        $this->type=$property;
    }

    public function setPrice($property) {
        $this->price=$property;
    }

    public function setSeat($property) {
        $this->seat=$property;
    }

    public function setSeller($property) {
        $this->seller=$property;
    }

    public function setPostedDate($property) {
        $this->posted_date=$property;
    }

    public function setAvailable($property) {
        $this->available=$property;
    }

}

?>