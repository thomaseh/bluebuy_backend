<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\Adticket;
use AppBundle\Form\AdticketType;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdticketsController extends FOSRestController
{
    /**
     *
     * @return AdTicket
     */
    public function getAdticketAction(Adticket $adticket)
    {
        return $adticket;
    }
    /**
     * @param $name
     * @return Adticket
     */
    public function getAdticketNameAction($name) {
        $adticket = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adticket')
            ->findBy(array('name' => $name, 'available' => 'Yes'));
        return $adticket;
    }
    /**
     * retrieve all adtickets
     * TODO: add pagination
     *
     * @return Adticket[]
     */
    public function getAdticketsAction()
    {
        $adtickets = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adticket')
            ->findBy(array('available' => 'Yes'));
        return $adtickets;
    }

    /**
     * @return \AppBundle\Entity\Adticket[]|array
     */
    public function getAdticketsSortbyTitleAction() {
        $adtickets = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adticket')
            ->findBy(array('available' => 'Yes'), array('name' => 'ASC'));
        return $adtickets;
    }

    /**
     * @return \AppBundle\Entity\Adticket[]|array
     */
    public function getAdticketsSortbyDateAction() {
        $adtickets = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adticket')
            ->findBy(array('available' => 'Yes'), array('event_date' => 'ASC'));
        return $adtickets;
    }

    /**
     * @return \AppBundle\Entity\Adticket[]|array
     */
    public function getAdticketsSortbyPriceAction() {
        $adtickets = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adticket')
            ->findBy(array('available' => 'Yes'), array('price' => 'ASC'));
        return $adtickets;
    }

    /**
     * @param Request $request
     * @return View
     */
    public function postAdticketsAction(Request $request)
    {
        //TODO: there's a simpler method using FOSRestBundle body converter
        // that's the reason why we need to be able to create
        // an adticket without body or title, to use it as
        // a placeholder for the form
        $adticket = new Adticket();
        $errors = $this->treatAndValidateRequest($adticket, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->persistAndFlush($adticket);
        // created => 201, we need View here because we're not
        // returning the default 200
        return new View($adticket, Response::HTTP_CREATED);
    }

    /**
     * @param Adticket $adticket
     * @param Request $request
     * @return View
     */
    public function putAdticketAction(Adticket $adticket, Request $request)
    {
        echo $request;
        // yes we replace totally the old adticket by a new one
        // except the id, because that's how PUT works
        // if you just want to "merge" you want to use PATCH, not PUT
        $id = $adticket->getId();
        $adticket = new Adticket();
        $adticket->setId($id);
        $errors = $this->treatAndValidateRequest($adticket, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->persistAndFlush($adticket);
        return new View($adticket, Response::HTTP_OK);
    }

    /**
     * @param Adticket $adticket
     * @param Request $request
     * @return View
     */
    public function deleteAdticketAction(Adticket $adticket, Request $request) {
        // delete
//        $id = $adticket->getId();
//        $adticket = new Adticket();
//        $adticket->setId($id);
        $errors = $this->treatAndValidateRequest($adticket, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->deleteAndFlush($adticket);
        return new View($adticket, Response::HTTP_OK);
    }
    /**
     * fill $adticket with the json send in request and validates it
     *
     * returns an array of errors (empty if everything is ok)
     *
     * @return array
     */
    private function treatAndValidateRequest(Adticket $adticket, Request $request)
    {
        // createForm is provided by the parent class
        $form = $this->createForm(
            'AppBundle\Form\AdticketType',
            $adticket,
            array(
                'method' => $request->getMethod()
            )
        );
        // this method is the one that will use the value in the POST
        // to update $adticket
        $form->handleRequest($request);

        // we use it like that instead of the standard $form->isValid()
        // because the json generated
        // is much readable than the one by serializing $form->getErrors()
        $errors = $this->get('validator')->validate($adticket);
        return $errors;
    }
    /**
     * @param Adticket $adticket
     */
    private function persistAndFlush(Adticket $adticket)
    {
        $manager = $this->getDoctrine()->getManager();
        $id = $adticket->getId();
        if (!is_numeric($id)) $id = -1;
        if ($id >= 0) {
            $product = $manager->getRepository('AppBundle:Adticket')->find($id);

            if (!$product) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $product->setEvent($adticket->getEvent());
            $product->setEventDate($adticket->getEventDate());
            $product->setTicketType($adticket->getTicketType());
            $product->setPrice($adticket->getPrice());
            $product->setSeat($adticket->getSeat());
            $product->setSeller($adticket->getSeller());
            $product->setPostedDate($adticket->getPostedDate());
            $product->setAvailable($adticket->getAvailable());
            $manager->flush();
        } else {
            $manager->persist($adticket);
            $manager->flush();
        }
    }

    /**
     * @param Adticket $adticket
     */
    private function deleteAndFlush(Adticket $adticket) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($adticket);
        $manager->flush();
    }
}