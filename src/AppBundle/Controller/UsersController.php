<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends FOSRestController
{
    /**
     *
     * @return User
     */
    public function getUserAction(User $user)
    {
        return $user;
    }
    /**
     * @param $name
     * @return User
     */
    public function getUserNameAction($email) {
        $user = $this
            ->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findBy(array('email' => $email));
        return $user;
    }
    /**
     * retrieve all users
     * TODO: add pagination
     *
     * @return User[]
     */
    public function getUsersAction()
    {
        $users = $this
            ->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();
        return $users;
    }

    public function getUsersSortbyEmailAction() {
        $users = $this
            ->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findBy(array(), array('email' => 'ASC'));
        return $users;
    }

    public function getUsersSalesAction($email) {
        $books = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adbook')
            ->findBy(array('seller' => $email, 'available' => 'Yes'));

        $leases = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adlease')
            ->findBy(array('seller' => $email, 'available' => 'Yes'));

        $tickets = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Adticket')
            ->findBy(array('seller' => $email, 'available' => 'Yes'));

        return array_merge(array_merge($books, $leases), $tickets);
    }

    /**
     * @param Request $request
     * @return View
     */
    public function postUsersAction(Request $request)
    {
        //TODO: there's a simpler method using FOSRestBundle body converter
        // that's the reason why we need to be able to create
        // an user without body or title, to use it as
        // a placeholder for the form
        $user = new User();
        $errors = $this->treatAndValidateRequest($user, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->persistAndFlush($user);
        // created => 201, we need View here because we're not
        // returning the default 200
        return new View($user, Response::HTTP_CREATED);
    }

    /**
     * @param User $user
     * @param Request $request
     * @return View
     */
    public function putUserAction(User $user, Request $request)
    {
        echo $request;
        // yes we replace totally the old user by a new one
        // except the id, because that's how PUT works
        // if you just want to "merge" you want to use PATCH, not PUT
        $id = $user->getId();
        $user = new User();
        $user->setId($id);
        $errors = $this->treatAndValidateRequest($user, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->persistAndFlush($user);
        return new View($user, Response::HTTP_OK);
    }

    /**
     * @param User $user
     * @param Request $request
     * @return View
     */
    public function deleteUserAction(User $user, Request $request) {
        // delete
//        $id = $user->getId();
//        $user = new User();
//        $user->setId($id);
        $errors = $this->treatAndValidateRequest($user, $request);
        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $this->deleteAndFlush($user);
        return new View($user, Response::HTTP_OK);
    }
    /**
     * fill $user with the json send in request and validates it
     *
     * returns an array of errors (empty if everything is ok)
     *
     * @return array
     */
    private function treatAndValidateRequest(User $user, Request $request)
    {
        // createForm is provided by the parent class
        $form = $this->createForm(
            'AppBundle\Form\UserType',
            $user,
            array(
                'method' => $request->getMethod()
            )
        );
        // this method is the one that will use the value in the POST
        // to update $user
        $form->handleRequest($request);

        // we use it like that instead of the standard $form->isValid()
        // because the json generated
        // is much readable than the one by serializing $form->getErrors()
        $errors = $this->get('validator')->validate($user);
        return $errors;
    }
    /**
     * @param User $user
     */
    private function persistAndFlush(User $user)
    {
        $manager = $this->getDoctrine()->getManager();
        $id = $user->getId();
        if (!is_numeric($id)) $id = -1;
        if ($id >= 0) {
            $product = $manager->getRepository('AppBundle:User')->find($id);

            if (!$product) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $product->setEmail($user->getEmail());
            $product->setPassword($user->getPassword());
            $manager->flush();
        } else {
            $manager->persist($user);
            $manager->flush();
        }
    }

    /**
     * @param User $user
     */
    private function deleteAndFlush(User $user) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($user);
        $manager->flush();
    }
}