<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160406224422 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE adlease (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(100) NOT NULL, leaseDate VARCHAR(10) NOT NULL, leaseStyle VARCHAR(50) NOT NULL, rooms VARCHAR(100) NOT NULL, description VARCHAR(500) NOT NULL, type VARCHAR(100) NOT NULL, price NUMERIC(10, 2) NOT NULL, seller VARCHAR(100) NOT NULL, postedDate VARCHAR(10) NOT NULL, available VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE lease');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lease (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, leaseDate VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, leaseStyle VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, rooms VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, description VARCHAR(500) NOT NULL COLLATE utf8_unicode_ci, type VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, price NUMERIC(10, 2) NOT NULL, seller VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, postedDate VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, available VARCHAR(5) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE adlease');
    }
}
